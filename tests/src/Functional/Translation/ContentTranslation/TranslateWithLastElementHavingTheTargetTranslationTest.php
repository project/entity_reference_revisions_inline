<?php

namespace Drupal\Tests\entity_reference_revisions_inline\Functional\Translation\ContentTranslation;

use Drupal\Tests\entity_reference_inline\Functional\Translation\ContentTranslation\TranslateWithLastElementHavingTheTargetTranslationTest as TranslateWithLastElementHavingTheTargetTranslationTestBase;

/**
 * Tests content translation for the inline entity reference widget.
 *
 * @group entity_reference_revisions_inline
 */
class TranslateWithLastElementHavingTheTargetTranslationTest extends TranslateWithLastElementHavingTheTargetTranslationTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = ['entity_reference_revisions_inline'];

  /**
   * {@inheritdoc}
   */
  protected function getEntityTypes() {
    $entityTypes = [
      0 => [
        0 => ['entity_type' => 'entity_test_mulrev', 'bundle' => 'entity_test_mulrev', 'field_name' => 'entity_reference_inline', 'field_type' => 'entity_reference_revisions_inline'],
        1 => ['entity_type' => 'entity_test_mulrev', 'bundle' => 'entity_test_mulrev', 'field_name' => 'entity_reference_inline', 'field_type' => 'entity_reference_revisions_inline'],
        2 => ['entity_type' => 'entity_test_mulrev', 'bundle' => 'entity_test_mulrev'],
      ],
    ];
    return $entityTypes;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntityTypesEnableContentTranslation() {
    $entityTypesEnableContentTranslation = [
      0 => [
        0 => ['entity_type' => 'entity_test_mulrev', 'bundle' => 'entity_test_mulrev'],
      ],
    ];
    return $entityTypesEnableContentTranslation;
  }

}
