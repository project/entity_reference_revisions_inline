<?php

namespace Drupal\entity_reference_revisions_inline\Plugin\DataType;

use Drupal\entity_reference_revisions\Plugin\DataType\EntityReferenceRevisions;
use Drupal\entity_reference_revisions_inline\Plugin\Field\FieldWidget\EntityReferenceRevisionsInlineWidget;

/**
 * Defines an 'entity_revision_inline_reference' data type.
 *
 * @DataType(
 *   id = "entity_revision_inline_reference",
 *   label = @Translation("Entity reference revisions inline"),
 *   definition_class = "\Drupal\Core\TypedData\DataReferenceDefinition"
 * )
 */
class EntityReferenceInlineRevisions extends EntityReferenceRevisions {

  /**
   * {@inheritdoc}
   */
  public function getTarget() {
    if (!isset($this->target) && isset($this->revision_id)) {
      if (($parent = $this->getParent()) && ($parent_entity = $parent->getEntity())) {

        $entity_type_id = $this->getTargetDefinition()->getEntityTypeId();
        $storage = \Drupal::entityTypeManager()
          ->getStorage($entity_type_id);

        if (isset($parent_entity->loadedUnchanged) && $parent_entity->loadedUnchanged) {
          // If the parent entity has been edited through the inline widget in
          // the default revision and we have to load the referenced entity
          // unchanged then in order to be in sync with the inline widget we
          // have to load the referenced entity unchanged in its default
          // revision instead in the currently referenced revision id by the
          // field item.
          if ($parent_entity->isDefaultRevision() && isset($parent_entity->inlineEditedLangcode)) {
            $id = isset($this->id) ? $this->id : $storage->loadRevision($this->revision_id)->id();

            $widget_built_entities = EntityReferenceRevisionsInlineWidget::$builtEntities;
            if (isset($widget_built_entities[$entity_type_id][$id])) {
              $widget_entity = $widget_built_entities[$entity_type_id][$id];
              // We need the widget loaded revision ID as in case of reused
              // entities which are already saved as a part of another entity
              // save as in this case ::getLoadedRevisionId() will return the
              // newer revision ID instead the one used by the widget which will
              // not allow us to detect changes.
              $widget_entity_revision_id = $widget_entity->widgetLoadedRevisionID;
              $entity = $storage->loadRevisionUnchanged($widget_entity_revision_id);
            }
            else {
              // @todo this case could never happen.
              $entity = $storage->loadUnchanged($id);
            }
          }
          else {
            // If we have a valid reference, return the entity's TypedData
            // adapter.
            //@todo this requires https://www.drupal.org/node/2620980
            /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
            $entity = $storage->loadRevisionUnchanged($this->revision_id);
          }

          if ($entity) {
            // Flag the entity object we are comparing that the entity has been
            // loaded through loadUnchanged in order for the referenced entities
            // hold in an inline entity field type to be loaded unchanged as well.
            // @see \Drupal\entity_reference_revisions_inline\Plugin\Field\FieldType\EntityReferenceRevisionsInlineItem::preSave()
            $entity->loadedUnchanged = TRUE;

            // Propagate the inline edited language code down the structure.
            $entity->inlineEditedLangcode = $parent_entity->inlineEditedLangcode;
          }

          $this->target = isset($entity) ? $entity->getTypedData() : NULL;
        }
        elseif (!empty($parent_entity->entityReferenceInlineForm)) {
          $id = isset($this->id) ? $this->id : $storage->loadRevision($this->revision_id)->id();
          $entity = $storage->load($id);
          $entity->entityReferenceInlineForm = TRUE;
          $this->target = $entity->getTypedData();
        }
      }
    }
    $target = isset($this->target) ? $this->target : parent::getTarget();
    return $target;
  }

}
