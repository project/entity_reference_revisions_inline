<?php

namespace Drupal\entity_reference_revisions_inline\Plugin\Field\FieldType;

use Drupal\entity_reference_inline\Plugin\Field\FieldType\FieldItemListCommonMethodsTrait;
use Drupal\entity_reference_revisions\EntityReferenceRevisionsFieldItemList;

/**
 * Defines a item list class for entity reference fields.
 */
class EntityReferenceRevisionsInlineFieldItemList extends EntityReferenceRevisionsFieldItemList implements EntityReferenceRevisionsInlineFieldItemListInterface {

  use FieldItemListCommonMethodsTrait;

//  /**
//   * {@inheritdoc}
//   */
//  public function preSave() {
//    parent::preSave();
//
//    /* Commenting out as it does not work in the case when the parent entity
//    is loaded and saved again without changes but with new revision set. In
//    this case this code will set again the rev translation affected flag on the
//    parent entity again, which is wrong. The code should be considered for
//    removal.
//
//    // If one of the referenced entity's translation has been changed meanwhile
//    // within the same revision we have to inherit the translation affected
//    // flag, as ::equals would not be able to detect always this change so that
//    // the storage could set correctly the flag for the parent entity.
//
//    // Avoid populating the value if it was already manually set.
//    $affected = $this->getEntity()->isRevisionTranslationAffected();
//    if (!isset($affected)) {
//      $langcode = $this->getLangcode();
//      foreach ($this as $delta => $item) {
//        if ($item->entity->hasTranslation($langcode) && $item->entity->getTranslation($langcode)->isRevisionTranslationAffected()) {
//          $this->getEntity()->setRevisionTranslationAffected(TRUE);
//          break;
//        }
//      }
//    }
//    */
//  }

}
