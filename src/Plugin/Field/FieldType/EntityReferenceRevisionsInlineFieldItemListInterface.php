<?php

namespace Drupal\entity_reference_revisions_inline\Plugin\Field\FieldType;

use Drupal\entity_reference_inline\Plugin\Field\FieldType\EntityReferenceInlineFieldItemListInterface;

/**
 * Interface for entity reference revisions inline lists of field items.
 */
interface EntityReferenceRevisionsInlineFieldItemListInterface extends EntityReferenceInlineFieldItemListInterface {}
