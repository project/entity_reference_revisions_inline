<?php

/**
 * @file
 * Contains \Drupal\entity_reference_revisions_inline\Plugin\Field\FieldType\EntityReferenceRevisionsInlineItem.
 */

namespace Drupal\entity_reference_revisions_inline\Plugin\Field\FieldType;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\TypedData\EntityDataDefinition;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataReferenceDefinition;
use Drupal\Core\TypedData\TranslatableInterface;
use Drupal\entity_reference_inline\Plugin\Field\FieldType\EntityReferenceInlineFieldItemListInterface;
use Drupal\entity_reference_inline\Plugin\Field\FieldType\FieldItemCommonMethodsTrait;
use Drupal\entity_reference_revisions\Plugin\Field\FieldType\EntityReferenceRevisionsItem;

/**
 * Defines the 'entity_reference_revisions_inline' entity field type.
 *
 * Supported settings (below the definition's 'settings' key) are:
 * - target_type: The entity type to reference. Required.
 *
 * @FieldType(
 *   id = "entity_reference_revisions_inline",
 *   label = @Translation("Entity reference revisions inline"),
 *   description = @Translation("An entity field containing an entity reference for inline editing."),
 *   category = @Translation("Reference revisions inline"),
 *   default_widget = "entity_reference_revisions_inline",
 *   default_formatter = "entity_reference_label",
 *   list_class = "\Drupal\entity_reference_revisions_inline\Plugin\Field\FieldType\EntityReferenceRevisionsInlineFieldItemList",
 * )
*/
class EntityReferenceRevisionsInlineItem extends EntityReferenceRevisionsItem implements EntityReferenceRevisionsInlineItemInterface {

  use FieldItemCommonMethodsTrait;

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = parent::propertyDefinitions($field_definition);

    $settings = $field_definition->getSettings();
    $target_type_info = \Drupal::entityTypeManager()->getDefinition($settings['target_type']);

    $properties['entity'] = DataReferenceDefinition::create('entity_revision_inline')
      ->setLabel($target_type_info->getLabel())
      ->setDescription(t('The referenced entity revision'))
      // The entity object is computed out of the entity ID.
      ->setComputed(TRUE)
      ->setReadOnly(FALSE)
      ->setTargetDefinition(EntityDataDefinition::create($settings['target_type']));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function setValue($values, $notify = TRUE) {
    if (isset($values) && !is_array($values)) {
      // If we are setting the entity then we have to update the target_id
      // and the target_revision_id property.
      $this->set('entity', $values, $notify);
      $this->writePropertyValue('target_id', $this->entity->id());
      $this->writePropertyValue('target_revision_id', $this->entity->getRevisionId());
    }
    else {
      parent::setValue($values, $notify);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getValue($include_computed = FALSE) {
    $values = FieldItemBase::getValue();
    if ($include_computed || $this->hasNewEntity()) {
      $values['entity'] = $this->entity;
    }
    return $values;
  }

  /**
   * {@inheritdoc}
   *
   * We save the entity only if it has the same revision id as the newest one in
   * the database. If it has translation changes then we inherit the changed
   * timestamp from the parent entity.
   *
   * If the entity is older than the one in the database  we only load the
   * unchanged entity from the database to update the references.
   *
   * @todo ensure that when calling getTranslationLanguages the first
   * returned language will be the language the parent entity was edit into and
   * thus preSave of its fields will first be called for this language.
   */
  public function preSave() {
    if ($this->skipPreSave) {
      return;
    }

    $entity_type_manager =  \Drupal::entityTypeManager();
    $target_entity_type = $this->getFieldDefinition()->getSetting('target_type');
    /** @var \Drupal\Core\Entity\ContentEntityStorageInterface $storage */
    $storage = $entity_type_manager->getStorage($target_entity_type);
    $entity_type = $entity_type_manager->getDefinition($target_entity_type);

    $parent = $this->getEntity();
    $parent_default_revision = $parent->isDefaultRevision();

    // Special handling when the entity is edited in only one language.
    if (isset($parent->inlineEditedLangcode) && ($inline_edited_langcode = $parent->inlineEditedLangcode)) {
      // If the field is translatable we have special handling to update the
      // new revision id on the other field translations if the referenced
      // entity is used over the other field items as well.
      if ($this->getFieldDefinition()->isTranslatable()) {
        // The field method is executed on the language in which the parent
        // entity is being saved first and then it is iterated over the other
        // languages.
        if ($inline_edited_langcode == $this->getLangcode()) {
          $this->doPreSave($parent_default_revision);
        }
        // Iterating over the other translations of the parent, which have
        // not been edited.
        else {
          // If the parent is in the default revision and the referenced entity
          // has been meanwhile saved in a newer revision (e.g. the previous if
          // branch) then we only exchange the entity without doing any entity
          // operations in order to save resources when having big structures
          // in multiple translations.
          if ($parent_default_revision) {
            $newest_revision_id = $storage->getQuery()->condition($entity_type->getKey('id'), $this->target_id)->execute();
            $newest_revision_id = key($newest_revision_id);

            // Only update it the target revision id has changed.
            if ($this->target_revision_id != $newest_revision_id) {
              $this->target_revision_id = $newest_revision_id;
            }
          }
          // If the parent is not in the default revision then perform regular
          // save.
          else {
            $this->doPreSave($parent_default_revision);
          }
        }
      }
      // For non-translatable fields perform the regular save.
      else {
        $this->doPreSave($parent_default_revision);
      }
    }
    // Otherwise if the entities are not flagged as being edited in only one
    // language then do the regular save.
    else {
      $this->doPreSave($parent_default_revision);
    }
  }

  /**
   * Helper method for ::preSave.
   *
   * @param bool $parent_default_revision
   *   Whether the parent is in the default revision or not.
   */
  protected function doPreSave($parent_default_revision) {
    // @todo find a way to check if the referenced entity is initialized,
    // as if not there is no point of saving it!

    /** @var ContentEntityInterface $referenced_entity */
    $referenced_entity = $this->entity;

    $needs_save = FALSE;
    if ($this->referencedEntityNeedsSave()) {
      if ($referenced_entity instanceof TranslatableInterface) {
        $referenced_entity = $this->getFieldDefinition()->isTranslatable() && $referenced_entity->hasTranslation($this->getLangcode()) ? $referenced_entity->getTranslation($this->getLangcode()) : $referenced_entity;
      }
      $needs_save = TRUE;
    }
    else {
      if (isset($referenced_entity->original) && ($unchanged = $referenced_entity->original) && isset($unchanged->loadedUnchanged)) {
        $unchanged = $referenced_entity->original;
      }
      else {
        /** @var \Drupal\Core\Entity\ContentEntityStorageInterface $storage */
        $storage = \Drupal::entityTypeManager()->getStorage($referenced_entity->getEntityTypeId());

        // We check the parent if it is in the default revisions as otherwise if
        // we iterate over the languages of a translatable field and load an
        // updated entity for the currently edited language in a previous
        // revision for which the target revision id has not been updated through
        // the field presave then it would be an old and not the default revision
        // anymore.
        if ($parent_default_revision) {
          // Get the entity id from the entity itself instead from the target_id
          // property, as if the entity set on the field item was new target_id
          // has been set to NULL, but meanwhile the entity might have been saved
          // elsewhere, but the target_id property will still not be set.
          /** @var \Drupal\Core\Entity\ContentEntityInterface $unchanged */
          $unchanged = $storage->loadUnchanged($referenced_entity->id());

          // If the referenced entity has been edited inline then flag the
          // unchanged entity accordingly in order for it to load inline
          // referenced entity unchanged in the default revision instead in the
          // referenced revision.
          // @see \Drupal\entity_reference_revisions_inline\Plugin\DataType\EntityReferenceInlineRevisions::getTarget()
          if (isset($referenced_entity->inlineEditedLangcode)) {
            $unchanged->inlineEditedLangcode = $referenced_entity->inlineEditedLangcode;
          }
        }
        else {
          /** @var \Drupal\Core\Entity\ContentEntityInterface $unchanged */
          $unchanged = $storage->loadRevisionUnchanged($referenced_entity->getLoadedRevisionId());
        }

        // Flag the entity object we are comparing that the entity has been
        // loaded through loadUnchanged in order for the referenced entities hold
        // in an inline entity field type to be loaded unchanged as well.
        // @see \Drupal\entity_reference_revisions_inline\Plugin\DataType\EntityReferenceInlineRevisions::getTarget()
        $unchanged->loadedUnchanged = TRUE;

        // As we've already loaded the unchanged entity we attach it to the
        // current referenced entity for performance reasons, because otherwise
        // it will be loaded again in EntityStorageBase::doPreSave or in
        // ContentEntityBase::hasTranslationChanges.
        $referenced_entity->original = $unchanged;
      }

      // In case of reused entities the referenced entity might've been already
      // saved, in which case we should skip saving it again.
      if (!empty($referenced_entity->inlineSaved) || ($referenced_entity->getRevisionId() > $unchanged->getRevisionId())) {
        $needs_save = FALSE;
      }
      // The referenced entity might not have the revision id at this point,
      // e.g. setNewRevision has been called on the referenced entity. That is
      // why we use the target revision id property instead of calling
      // getRevisionId on the referenced entity.
      elseif ($this->target_revision_id == $unchanged->getRevisionId()) {
        // We save the entity only in case it has changed in order to speed up
        // the save process of big structures.

        // A fast check for addition or removal of an entity translation.
        $current_translation_langcodes = array_keys($referenced_entity->getTranslationLanguages());
        $previous_translation_langcodes = array_keys($unchanged->getTranslationLanguages());
        sort($current_translation_langcodes);
        sort($previous_translation_langcodes);
        if ($current_translation_langcodes != $previous_translation_langcodes) {
          if ($this->getFieldDefinition()->isTranslatable() && $referenced_entity->hasTranslation($this->getLangcode())) {
            $referenced_entity = $referenced_entity->getTranslation($this->getLangcode());
          }
          $referenced_entity->inlineEditedHasTranslationChanges = TRUE;
          $needs_save = TRUE;
        }
        else {
          // If the entity has been edited through our inline widget then it
          // might have been flagged with the translation language in which it
          // has been edited, so we have to check only this translation for
          // changes.
          // @see \Drupal\entity_reference_inline\Plugin\Field\FieldWidget\EntityReferenceInlineWidget::buildEntity()
          if (isset($referenced_entity->inlineEditedLangcode) && ($inline_edited_langcode = $referenced_entity->inlineEditedLangcode) && $referenced_entity->hasTranslation($inline_edited_langcode)) {
            $referenced_entity = $referenced_entity->getTranslation($inline_edited_langcode);
            if ($referenced_entity->hasTranslationChanges()) {
              $referenced_entity->inlineEditedHasTranslationChanges = TRUE;
              $needs_save = TRUE;
            }
          }
          else {
            foreach ($current_translation_langcodes as $langcode) {
              /** @var \Drupal\Core\Entity\ContentEntityInterface $translation */
              $translation = $referenced_entity->getTranslation($langcode);
              if ($translation->hasTranslationChanges()) {
                if ($this->getFieldDefinition()->isTranslatable() && $referenced_entity->hasTranslation($this->getLangcode())) {
                  $referenced_entity = $referenced_entity->getTranslation($this->getLangcode());
                }
                elseif ($translation->isNewTranslation()) {
                  $referenced_entity = $translation;
                }
                $referenced_entity->inlineEditedHasTranslationChanges = TRUE;
                $needs_save = TRUE;
                break;
              }
            }
          }
        }
      }
      // Exchange the referenced entity with the unchanged only if we are
      // saving the default revision of the parent entity, otherwise if loading
      // and saving a previous revision we'll change its reference, which is
      // desired only when working with the default revision.
      elseif ($parent_default_revision) {
        $referenced_entity = $unchanged;
      }
      else {
        // Something is wrong.
      }
    }

    // In case we haven't found any changes we still have to check the
    // underlying structure for inconsistencies which require save of the
    // current entity.
    if ($needs_save || $this->needsSave(FALSE)) {
      // Flag the entity as being in a saving process. If there are inline
      // references on the entity as well then when calling
      // hasTranslationChanges on their item list we will append the original
      // entity and in order to not unset it and so reset the cache of the
      // Entity::hasTranslationChanges and also to not load later again
      // additionally the original again we will check if the parent is in a
      // saving process and if so do not unset the original entity, otherwise
      // we do unset it.
      // @see EntityReferenceRevisionsInlineFieldItemList::hasTranslationChanges
      $referenced_entity->inlineSaving = TRUE;

      // Save the entity.
      $referenced_entity->inlineSaved = TRUE;
      $referenced_entity->save();
    }

    // We have to always update the properties even if we haven't saved the
    // entity here, but in case of a reused entity it might have been saved as
    // part of the preSave of other translation, field or field item.
    $this->entity = $referenced_entity;
    $this->writePropertyValue('target_id', $this->entity->id());
    $this->writePropertyValue('target_revision_id', $this->entity->getRevisionId());
  }

  /**
   * Checks whether the item needs property updates.
   *
   * This method checks if the property holding the entity ID matches the entity
   * ID on the entity object i.e. if the property is up to date. This might
   * happen if somehow the ID of the entity object is changed.
   *
   * This method checks additionally if the property holding the referenced
   * revision ID differs from the revision ID of the entity object. This might
   * happen if the entity is saved a got a new revision ID.
   *
   * @return bool
   */
  protected function itemNeedsPropertiesUpdate() {
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $this->entity;
    if (!$entity) {
      return isset($this->target_id) || isset($this->target_revision_id);
    }

    // Compare with ::getLoadedRevisionId(), as if ::setNewRevision(TRUE) is
    // called, then ::getRevisionId() will return NULL.
    return ($this->target_revision_id != $entity->getLoadedRevisionId()) || ($this->target_id != $entity->id());
  }

  /**
   * Checks whether the referenced entity needs save.
   *
   * @return bool
   */
  protected function referencedEntityNeedsSave() {
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $this->entity;
    if (!$entity) {
      return FALSE;
    }

    $needs_save = $entity->isNew() || $entity->isNewTranslation() || $entity->isNewRevision() || !empty($entity->needsSave);
    return $needs_save;
  }

  /**
   * {@inheritdoc}
   */
  public function needsSave($include_current_item = TRUE) {
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $this->entity;
    if (!$entity) {
      return FALSE;
    }

    $needs_save = $include_current_item ? ($this->needsSave || $this->referencedEntityNeedsSave() || $this->itemNeedsPropertiesUpdate()) : FALSE;
    if (!$needs_save) {
      foreach (array_keys($entity->getTranslationLanguages()) as $langcode) {
        $translation = $entity->getTranslation($langcode);
        if ($translation->isNewTranslation()) {
          // For performance reasons we set the flag on the item, so that in
          // case of a big structure not to iterate down the whole structure
          // till the bottom each time we go one level deeper.
          $this->needsSave = TRUE;
          return TRUE;
        }

        // We have to go down the structure and check if an entity down the
        // structure needs a save. Such a check is needed for e.g. reused
        // entities which have been saved on an item of one entity, but are also
        // appended on an item of another entity and the inline reference fields
        // of this another entity have to be checked for inconsistencies e.g.
        // revision ID updated on the refernced entity but not yet on the item
        // referencing it so that this other entity gets to reference the new
        // revision ID during the save.
        $fields = $translation->isDefaultTranslation() ? $translation->getFields() : $translation->getTranslatableFields();
        foreach ($fields as $items) {
          if ($items instanceof EntityReferenceInlineFieldItemListInterface) {
            if ($items->needsSave()) {
              // For performance reasons we set the flag on the item, so that in
              // case of a big structure not to iterate down the whole structure
              // till the bottom each time we go one level deeper.
              $this->needsSave = TRUE;
              return TRUE;
            }
          }
        }
      }
    }
    elseif (!$this->needsSave) {
      // For performance reasons we set the flag on the item, so that in case of
      // a big structure not to iterate down the whole structure till the bottom
      // each time we go one level deeper.
      $this->needsSave = TRUE;
    }

    return $needs_save;
  }

}
