<?php

/**
 * @file
 * Contains \Drupal\entity_reference_revisions_inline\Plugin\Field\FieldType\EntityReferenceRevisionsInlineAutocompleteWidget.
 */

namespace Drupal\entity_reference_revisions_inline\Plugin\Field\FieldWidget;

use Drupal\entity_reference_revisions\Plugin\Field\FieldWidget\EntityReferenceRevisionsAutocompleteWidget;

/**
 * Plugin implementation of the 'entity_reference_revisions_inline_autocomplete' widget.
 *
 * @FieldWidget(
 *   id = "entity_reference_revisions_inline_autocomplete",
 *   label = @Translation("Autocomplete"),
 *   description = @Translation("An autocomplete text field."),
 *   field_types = {
 *     "entity_reference_revisions_inline"
 *   }
 * )
 */
class EntityReferenceRevisionsInlineAutocompleteWidget extends EntityReferenceRevisionsAutocompleteWidget {

}
