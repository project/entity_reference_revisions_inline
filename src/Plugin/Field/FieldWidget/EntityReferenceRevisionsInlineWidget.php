<?php

/**
 * @file
 * Contains \Drupal\entity_reference_revisions_inline\Plugin\Field\FieldWidget\EntityReferenceRevisionsInlineWidget.
 */

namespace Drupal\entity_reference_revisions_inline\Plugin\Field\FieldWidget;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\entity_reference_inline\Plugin\Field\FieldWidget\EntityReferenceInlineWidget;

/**
 * Plugin implementation of the 'entity_reference_revisions_inline' widget.
 *
 * @FieldWidget(
 *   id = "entity_reference_revisions_inline",
 *   label = @Translation("Entity reference revisions inline widget"),
 *   description = @Translation("An inline entity form of the referenced entities."),
 *   entity_deep_serialization = TRUE,
 *   field_types = {
 *     "entity_reference_revisions_inline"
 *   }
 * )
 */
class EntityReferenceRevisionsInlineWidget extends EntityReferenceInlineWidget {

  /**
   * {@inheritdoc}
   */
  protected function addEntityMetaInformation(ContentEntityInterface $entity, &$element) {
    $rev_id_field_name = $entity->getEntityType()->getKey('revision');
    parent::addEntityMetaInformation($entity, $element);
    $element[$rev_id_field_name] = [
      '#type' => 'hidden',
      '#value' => $entity->getRevisionId()
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function loadEntityFromMetaInformation($values) {
    $rev_id_field_name = $this->getReferencedEntityType()->getKey('revision');
    $entity = NULL;
    if (isset($values[$rev_id_field_name])) {
      $entity = $this->getReferencedEntityStorage()->loadRevision($values[$rev_id_field_name]);
    }
    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  protected function setBuiltEntity(EntityInterface $entity) {
    // We set a dynamic property on the entity object holding the revision ID
    // used in the form so that we still could find the original entity in the
    // field saving process in case of reused entities where the entity is saved
    // on one of the items referencing it and afterwards the preSave of the
    // other items is invoked.
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity->widgetLoadedRevisionID = $entity->getLoadedRevisionId();
    parent::setBuiltEntity($entity);
  }

}
