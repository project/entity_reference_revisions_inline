<?php

/**
 * @file
 * Contains \Drupal\entity_reference_revisions_inline\Plugin\Field\FieldFormatter\EntityReferenceRevisionsInlineEntityFormatter.
 */

namespace Drupal\entity_reference_revisions_inline\Plugin\Field\FieldFormatter;

use Drupal\entity_reference_inline\Plugin\Field\FieldFormatter\FieldFormatterCommonMethodsTrait;
use Drupal\entity_reference_revisions\Plugin\Field\FieldFormatter\EntityReferenceRevisionsEntityFormatter;

/**
 * Plugin implementation of the 'entity reference rendered entity' formatter.
 *
 * @FieldFormatter(
 *   id = "entity_reference_revisions_inline_entity_view",
 *   label = @Translation("Rendered entity"),
 *   description = @Translation("Display the referenced entities rendered by entity_view()."),
 *   field_types = {
 *     "entity_reference_revisions_inline"
 *   }
 * )
 */
class EntityReferenceRevisionsInlineEntityFormatter extends EntityReferenceRevisionsEntityFormatter {

  use FieldFormatterCommonMethodsTrait;

}
